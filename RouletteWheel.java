package Activity1.roulette; 
import java.util.Random;

public class RouletteWheel
{
    private Random randSpin;
    private int spinNum;

    public RouletteWheel()
    {
        this.randSpin = new Random();
        this.spinNum = 0;
    }

    public void spin()
    {
        this.spinNum = this.randSpin.nextInt(37);
    }

    public int getValue()
    {
        return this.spinNum;
    }

    public boolean isLow()
    {
        if (this.spinNum <= 18)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isHigh()
    {
        if (this.spinNum >= 19)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isEven()
    {
        if (this.spinNum > 0 && this.spinNum % 2 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isOdd()
    {
        if (this.spinNum > 0 && this.spinNum % 2 != 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isRed()
    {
        if ((this.spinNum > 0 && this.spinNum < 11 || this.spinNum > 18 && this.spinNum < 29) && this.spinNum % 2 != 0)
        {
            return true;
        }
        else if ((this.spinNum > 10 && this.spinNum < 19 || this.spinNum > 28 && this.spinNum < 37) && this.spinNum % 2 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isBlack()
    {
        if (this.isRed())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}