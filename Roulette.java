package Activity1.roulette;
import java.util.Scanner;

public class Roulette
{
    public static void main(String[] args)
    {
        playTheGame();
    }

    private static void playTheGame()
    {
        Scanner input = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        boolean playing;
        int numBetOn;
        int bet;
        int total = 0;
        int gamesPlayed = 0;

        do
        {
            System.out.println("Would you like to make a bet?(true/false)");
            playing = input.nextBoolean();
            
            if (playing)
            {
                gamesPlayed++;
                System.out.println("What number would you like to bet on?(0-36)");
                numBetOn = input.nextInt();

                System.out.println("How much money would you like to bet?");
                bet = input.nextInt();

                wheel.spin();

                if (numBetOn == wheel.getValue())
                {
                    System.out.println("You won! Adding " + (bet * 35) + " to your total.");
                    total = total + (bet * 35);
                }
                else
                {
                    System.out.println("You lose... ):");
                    total = total - bet;
                }
            }
        } while (playing);
        if (gamesPlayed > 0)
        {
            System.out.println("Thank you for playing, your total earnings are " + total);
        }
    }
}